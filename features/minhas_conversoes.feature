# language: pt

@conversoes_customizadas
Funcionalidade: Como um usuário quero poder criar conversões

  @conversao
  Cenário: Criar uma conversão
    Dado que esteja na home no app
    E pressionar no botão de menu
    E pressionar o botão de minhas conversões
    E clicar em criar nova conversão
    E digitar "Forca" como nome da conversão
    Quando clicar no botão de nova unidade
    E digitar "TESTE 1" como nome da unidade
    E digitar "T1" como simbolo da unidade
    E digitar "100" como valor da unidade
    E clicar no botão de submit para salvar a conversão
    Quando clicar no botão de nova unidade
    E digitar "TESTE 2" como nome da unidade
    E digitar "T2" como simbolo da unidade
    E digitar "50" como valor da unidade
    E  clicar no botão de submit para salvar a conversão
    Quando pressionar no botão OK
    Então verifico "Forca" foi adicionada a minha lista de conversões
