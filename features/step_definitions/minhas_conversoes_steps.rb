Dado("clicar em criar nova conversão") do
  find_element(id: "btn_new_custom_conversion").click
end

Dado("digitar {string} como nome da conversão") do |nome|
  find_element(id: "edit_custom_conversion_category_name").click
  find_element(id: "edit_custom_conversion_category_name").send_keys(nome)
end

Quando("clicar no botão de nova unidade") do
  find_element(id: "btn_new_custom_unit").click
end

Quando("digitar {string} como nome da unidade") do |nome_unidade|
  find_element(id: "edit_custom_conversion_unit_dtls_name").send_keys(nome_unidade)
end

Quando("digitar {string} como simbolo da unidade") do |nome_simbolo|
  find_element(id: "edit_custom_conversion_unit_dtls_symbol").send_keys(nome_simbolo)
end

Quando("digitar {string} como valor da unidade") do |valor_unidade|
  find_element(id: "edit_custom_conversion_unit_dtls_value").send_keys(valor_unidade)
end

Quando("clicar no botão de submit para salvar a conversão") do
  find_element(id: "action_confirm_custom_unit").click
end

Quando("pressionar no botão OK") do
  find_element(id: "btn_custom_conversion_details_ok").click
end

Então("verifico {string} foi adicionada a minha lista de conversões") do |nome_conversao|
  text(nome_conversao)
end
