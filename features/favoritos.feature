# language: pt

@favoritos
Funcionalidade: Como um usuário quero poder gerenciar meus favoritos

  @adiciona_favorito
  Cenário: Adiconar um favorito a lista
    Dado que esteja na home no app
    E pressionar no botão de menu
    Quando pressiono para adicionar aos favoritos
    E acesso a lista de favoritos
    Então verifico 'Área' foi adicionado a minha lista com sucesso

  @deleta_favorito
  Cenário: Excluir um favorito da lista
    Dado que esteja na home no app
    E pressionar no botão de menu
    E pressiono para adicionar aos favoritos
    E acesso a lista de favoritos
    Quando clicar para remover o favorito
    Então verifico que ele foi removido com sucesso