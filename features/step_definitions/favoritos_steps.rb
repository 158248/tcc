Quando("pressiono para adicionar aos favoritos") do
  text("Área").click
  find_element(id: ("action_add_favorites")).click
end

Quando("acesso a lista de favoritos") do
  find_element(accessibility_id: ("Gaveta de navegação abrir")).click
  text("Conversões favoritos").click
end

Então("verifico {string} foi adicionado a minha lista com sucesso") do |favorito|
  selecionado = find_element(id: ("favorites_item_hint")).text

  fail("Erro - esperava #{favorito}, retornou #{selecionado}") if selecionado != favorito
end

Quando("clicar para remover o favorito") do
  find_element(id: ("deleteIcon")).click
end

Então("verifico que ele foi removido com sucesso") do
  find_element(id: ("text_info_favorites")).click
end
