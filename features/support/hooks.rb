require "appium_lib"
require "cucumber"
require "pry"

Before do
  $driver.start_driver
end

# screenchots
After do |scenario|
  time_stamp = Time.now.strftime('%Y-%m-%d_%H.%M.%S')
  Dir.mkdir('report') unless Dir.exist?('report')
  sufix = ('fail' if scenario.failed?) || 'sucess'
  name = scenario.name.tr(' ', '_').downcase
  screenshot_file = screenshot("report/#{sufix}_#{name}_#{time_stamp}.png")
  embed(screenshot_file.to_s, 'image/png')
  $driver.driver_quit
end

# exlcui pasta de reports depois da execução
AfterConfiguration do
  FileUtils.rm_r('report') if File.directory?('report')
end

# método que faz o scroll até o elemento desejado
def scroll_element(valor)
  3.times { Appium::TouchAction.new.swipe(star_x: 0.5, start_y: 0.2 , end_x: 0.5, end_y: 0.8, duration: 1000).perform }

  current_screen = get_source
  previous_screen = ''

  until exists{ text(valor) } || (current_screen == previous_screen) do
    Appium::TouchAction.new.swipe(star_x: 0.5, start_y: 0.8 , end_x: 0.5, end_y: 0.4, duration: 1000).perform
    previous_screen = current_screen
    current_screen = get_source
  end

  if exists{ text(valor) }
    text(valor).click
  else
    fail("Não foi possível encontrar o elemento #{valor}")
  end
end