Dado("que esteja na home no app") do
  find_element(id: ("action_search"))
end

Quando("botão show all deve estar {string}") do |status|
  botao_show_all = find_element(id: "btn_show_all").enabled?

  if status.eql?('habilitado')
    fail("Erro - esperava botão habilitado") if botao_show_all != true
  elsif status.eql?('desabilitado')
    fail("Erro - esperava botão desabilitado") if botao_show_all != false
  end
end

Quando("pressionar no botão de menu") do
  find_element(accessibility_id: ("Gaveta de navegação abrir")).click
end

Dado("pressionar o botão de minhas conversões") do
  text("Meus conversões").click
end

Então("verificar que aba do menu é aberta") do
  text("Conversor de Unidades")
end

Dado("pressionar no botão de pesquisa") do
  find_element(id: "action_search").click
end

Quando("digitar {string} no campo de busca") do |conversor|
  find_element(id: "search_src_text").send_keys(conversor)
end

Quando("pressionar no botão próximo do teclado android") do
  Appium::TouchAction.new.tap(x:0.99, y:0.99, count: 1).perform
end

Então("verificar {string} na tela atual") do |current_unit|
  find_element(id: "action_bar").find_element(xpath: "//android.widget.TextView[@text='#{current_unit}']")
end

Então("verificar que unidade da esquerda é {string}") do |valor_esquerda|
  valor_atual = find_elements(id: "select_unit_spinner")[0].text

  fail("Erro - esperava valor: #{valor_esquerda}, retornou: #{valor_atual}") if valor_atual != valor_esquerda
end

Então("verificar que unidade da direita é {string}") do |valor_direita|
  valor_atual = find_elements(id: "select_unit_spinner")[1].text

  fail("Erro - esperava valor: #{valor_atual}, retornou: #{valor_direita}") if valor_atual != valor_direita
end

Quando("digitar {string} no teclado do aplicativo") do |valor|
  digito = valor.split("")

  digito.each do |num|
    find_element(id: "keypad").find_element(xpath: "//android.widget.Button[@text='#{num}']").click
  end
end

Então("verificar o resultado esperado {string}") do |resultado|
  valor = find_element(id: "target_value").text

  fail("Erro - esperava valor: #{resultado}, retornou: #{valor}") if valor != resultado
end

Quando("selecionar {string} no campo da esquerda") do |valor_esquerda|
  find_elements(id: "select_unit_spinner")[0].click
  scroll_element(valor_esquerda)
end

Dado("seleciono {string} no menu") do |valor|
  text(valor).click
end

Quando("selecionar {string} no campo da direita") do |valor_direita|
  find_elements(id: "select_unit_spinner")[1].click
  scroll_element(valor_direita)
end

Quando("pressionar o botão para trocar as unidades de lado") do
  find_element(id: "img_switch").click
end

Quando("arrastar tela para {string}") do |string|
  string.eql?('esquerda') ? Appium::TouchAction.new.swipe(start_x: 0.01, start_y: 0.5, end_x: 0.8, end_y: 0.5, duration:600).perform : Appium::TouchAction.new.swipe(start_x: 0.99 , start_y: 0.5, end_x: 0.01, end_y: 0.5, duration:600).perform
end