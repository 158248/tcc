# language: pt

@main_menu
Funcionalidade: Como usuário quero poder converter unidades

  @home
  Cenário: Visualizar barra de menu
    Dado que esteja na home no app
    Quando pressionar no botão de menu
    Então verificar que aba do menu é aberta

  @botao_show_all
  Cenário: Validar visibildiade do botão show all
    Dado que esteja na home no app
    Quando botão show all deve estar 'desabilitado'
    E digitar '1' no teclado do aplicativo
    Então botão show all deve estar 'habilitado'

  @pesquisa
  Cenário: Pesquisar por tipos de conversões existentes
    Dado que esteja na home no app
    E pressionar no botão de pesquisa
    Quando digitar "Temperatura" no campo de busca
    E pressionar no botão próximo do teclado android
    Então verificar "Temperatura" na tela atual
    E verificar que unidade da esquerda é "Celsius"
    E verificar que unidade da direita é "Fahrenheit"

  @conversao_default
  Esquema do Cenário: Verificar conversão default
    Dado que esteja na home no app
    Quando digitar <digito> no teclado do aplicativo
    Então verificar o resultado esperado <resultado>

    Exemplos:
      | digito | resultado |
      | "1"    | "30.48"   |
      | "2"    | "60.96"   |
      | "3"    | "91.44"   |
      | "9"    | "274.32"  |
      | "10"   | "304.8"   |

  @selecionar_unidade
  Esquema do Cenário: Selecionar valor da unidade desejada
    Dado que esteja na home no app
    Quando selecionar <unidade> no campo da esquerda
    E digitar <digito> no teclado do aplicativo
    Então verificar o resultado esperado <resultado>

    Exemplos:
      | unidade    | digito | resultado |
      | "Polegada" | "1"    | "2.54"    |
      | "Mão"      | "2"    | "20.32"   |

  @selecionar_unidade_menu
  Esquema do Cenário: Selecionar valor da unidade desejada de um tipo de conversão do menu
    Dado que esteja na home no app
    E pressionar no botão de menu
    E seleciono 'Volume' no menu
    Quando selecionar <unidade> no campo da direita
    E digitar <digito> no teclado do aplicativo
    Então verificar o resultado esperado <resultado>

    Exemplos:
      | unidade | digito | resultado |
      | "Copo"  | "1"    | "15.1416" |
      | "Balde" | "2"    | "0.4163"  |

  @trocar_unidades_lado
  Cenário: Trocar de lado as unidades utilizando o botão
    Dado que esteja na home no app
    E pressionar no botão de pesquisa
    E verificar que unidade da esquerda é "Pé"
    E verificar que unidade da direita é "Centímetro"
    Quando pressionar o botão para trocar as unidades de lado
    Então verificar que unidade da esquerda é "Centímetro"
    E verificar que unidade da direita é "Pé"

  @trocar_tela_scroll_horizontal
  Cenário: Gestos de arrastar a tela horizontalmente
    Dado que esteja na home no app
    Quando arrastar tela para 'direita'
    E verificar "Calculadora" na tela atual
    Então arrastar tela para 'esquerda'
    E verificar "Conversor de Unidades" na tela atual